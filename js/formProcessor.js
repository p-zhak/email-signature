$(function () {
    var companies = {};

    var companyAvatarsSelect = "#avenger-form select[name=avatar]";
    var companyNamesSelect = "#avenger-form select[name=company_name]";
    var companyForm = "#avenger-form";
    var preview = "#avenger-preview";

    var selectedCompany = {};

    function getJson() {
        $.ajax({
            url: 'js/avengers.json',
            datatype: 'json',
            success: function (data) {
                //companies = data.avengers;
                initCompanies(data.avengers)
            },
        });
    }

    function initCompanies(data) {
        companies = data;
        companyAvatarsOptionList( companies );
        companyOptionList( companies );
        fillDefaultData( getRandomCompany() );
    }

    function getRandomCompany() {
        var keys = Object.keys(companies);
        var random = keys[(Math.random() * keys.length) >> 0];

        return companies[random];
    }

    function companyAvatarsOptionList( companies ) {
        $(companyAvatarsSelect).html('');
        $.each(companies, function (i, e) {
            $(companyAvatarsSelect).append('<option value="' + e.avatar + '">' + e.company_name + '\'s avatar</option>');
        });
    }

    function companyOptionList( companies ) {
        $(companyNamesSelect).html('');
        $.each(companies, function (i, e) {
            $(companyNamesSelect).append('<option value="' + e.company_name + '">' + e.company_name + '</option>');
        });
    }

    function fillDefaultData(company) {
        $('#avenger-form').find('input, select, textarea').each(function () {
            var $this = $(this);

            var name = $this.attr('name');

            if (company[name]) {
                $this.val(company[name]);
            }
        });
    }

    function changeCompany() {
        $(companyNamesSelect).on('change', function () {
            var companyName = this.value;
            $.each(companies, function (i, e) {
                if(e.company_name === companyName) {
                    selectedCompany = e;
                }
            });

            if(selectedCompany.company_name == "Trujay") { $('[data-attr="preview-footer-text"]').css('display', 'none'); }
            else { $('[data-attr="preview-footer-text"]').css('display', 'block'); }

            fillDefaultData( selectedCompany );
            fillCompanyAvatar( selectedCompany );
            checkEmptySocials( selectedCompany );
        });
    }

    function checkEmptySocials( company ) {
        if(company.facebook_profile.length <= 0) { $(preview).find('[data-preview="facebook_profile"]').css('display', 'none'); } else { $(preview).find('[data-preview="facebook_profile"]').css('display', 'inline'); }
        if(company.twitter_profile.length <= 0) { $(preview).find('[data-preview="twitter_profile"]').css('display', 'none'); } else { $(preview).find('[data-preview="twitter_profile"]').css('display', 'inline'); }
        if(company.linkedin_profile.length <= 0) { $(preview).find('[data-preview="linkedin_profile"]').css('display', 'none'); } else { $(preview).find('[data-preview="linkedin_profile"]').css('display', 'inline'); }
    }

    function fillCompanyAvatar( company ) {
        $(preview).find('[data-preview="website_link"]').each(function (i, e) {
            $(this).attr('href', company.website_link);
        });
        $(preview).find('[data-preview="website"]').each(function (i, e) {
            $(this).text(company.website);
        });
        $(preview).find('[data-preview="avatar"]').each(function (i, e) {
            $(this).attr('src', company.avatar);
            $(this).attr('alt', company.company_name);
        });
        $(preview).find('[data-preview="company_name"]').html(company.company_name);
    }

    $(companyForm).find('[name="fullname"]').on('keyup', function () {
        var data = this.value;
        $(preview).find('[data-preview="fullname"]').html(data);
    });

    $(companyForm).find('[name="position"]').on('keyup', function () {
        var data = this.value;
        $(preview).find('[data-preview="position"]').html(data);
    });

    function getIdFromUrl(url) { return url.match(/[-\w]{25,}/); }

    $(companyForm).find('[name="user_avatar"]').on('keyup', function () {
        var data = this.value;
        if(data.length > 0) {
            var fileId = getIdFromUrl(data);
            $(preview).find('[data-preview="avatar"]').attr('src', 'https://drive.google.com/uc?export=view&id=' + fileId);
        } else {
            $(preview).find('[data-preview="avatar"]').attr('src', selectedCompany.avatar);
        }
    });

    $(companyForm).find('[name="personal_phone"]').on('keyup', function () {
        var data = this.value;
        $(preview).find('[data-preview="corporate_phone_link"]').html(data);
        if(data.length <= 0) {
            $(preview).find('[data-preview="corporate_phone_link"]').parent().css('display', 'none');
        } else {
            $(preview).find('[data-preview="corporate_phone_link"]').parent().css('display', 'block');
        }
    });

    $(companyAvatarsSelect).on('change', function () {
        var img = this.value;
        $(preview).find('[data-preview="avatar"]').each(function (i, e) {
            $(this).attr('src', img);
        });
    });

    $(companyForm).find('[name="email"]').on('keyup', function () {
        var data = this.value;
        $(preview).find('[data-preview="footer-link"]').attr('href', 'https://www.magneticone.com/?utm_source=email_signature&utm_medium=partof_link&utm_campaign=' + data);
        $(preview).find('[data-preview="website_link"]').attr('href', selectedCompany.website_link + '?utm_source=email_signature&utm_medium=partof_link&utm_campaign=' + data);
    });

    $('button[name="copy2clipboard"]').on('click', function () {
        var container_preview = document.getElementById('avenger-preview');
        var range = new Range();

        range.setStart(container_preview, 0);
        range.setEnd(container_preview, 2);

        document.getSelection().removeAllRanges();
        document.getSelection().addRange(range);
        document.execCommand("copy");
    });

    $('button[name="copy2clipboard_html"]').on('click', function () {
        var html = $('div#avenger-preview').html();
        $('input#clipboardTemp').val(html).select();
        document.execCommand("copy");
    });


    getJson();
    changeCompany();
});
